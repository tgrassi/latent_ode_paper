import numpy as np
import os
from network import Network
from solver import Solver
from glob import glob
from time import time


# remove pngs and output data
for g in ["pngs/*.png", "outputs/*.dat"]:
    for fname in glob(g):
        os.unlink(fname)


only = "H, H+, H2, H2+, H3+, O, O+, OH+, OH, O2, O2+, H2O, \
        H2O+, H3O+, C, C+, CH, CH+, CH2, CH2+, CH3, CH3+, \
        CH4, CH4+, CH5+, CO, CO+, HCO+, He, He+, E"
only_list = [x.strip() for x in only.split(",")]

net = Network("osu_09_2008.dat", only=only)
sol = Solver(net)

spy = 365.*24.*3600.

dt = 1e8 * spy
ngas = 1e4
tgas = 5e1
crate = 1e-16

net.species_list_to_file("species_list.py")

# log of random abundance range
rmax = -4.
rmin = -6.

average_integration_time = 0e0

for imod in range(500):
    n = {k: 1e-20*ngas for k in only_list}

    n["H2"] = ngas
    n["C"] = ngas * 1e1**(np.random.random() * (rmax - rmin) + rmin)
    n["C+"] = ngas * 1e1**(np.random.random() * (rmax - rmin) + rmin)
    n["O"] = (n["C"] + n["C+"]) * 4

    print(imod, n["C"], n["O"], n["C+"], n["O+"])

    n["E"] = sum([n[x] if "+" in x else 0e0 for x in only_list])

    n = {k: np.log10(v) for k, v in n.items()}

    time_start = time()
    sol.solve(n, dt, tgas, crate, tsteps=100, atol=1e-12, rtol=1e-8)
    integration_time = time() - time_start
    average_integration_time += integration_time
    print("integration time: %.2f s, (avg: %.2f)" % (integration_time, average_integration_time / (imod + 1)))

    sol.plot_above(nmin=-5, fname="pngs/chemistry_%04d.png" % imod)

    sol.results_to_file("outputs/chemistry_%04d.dat" % imod)


