import numpy as np


class Network:
    # ************************
    def __init__(self, fname, only=None, verbose=1, fkeep=1e0, preprocess=True):
        self.reactions = []
        self.species = []
        self.species_idx = []
        self.verbose = verbose
        self.load_osu(fname, only, fkeep)
        self.find_species()

        if preprocess:
            self.preprocess_commons()
            self.preprocess_fex()
            self.preprocess_rates()

    # ************************
    def load_osu(self, fname, only, fkeep):

        if only is not None:
            if "," in only:
                only = [x.strip() for x in only.split(",")]

        for row in open(fname):
            srow = row.strip()
            if srow == "" or srow.startswith("#"):
                continue
            fmt = [8] * 3 + [8] * 5 + [9] * 3 + [2]
            wht = "R" * 3 + "P" * 5 + "abc" + "i"
            uwht = list(set(wht))

            t = 0
            rate = {w: [] for w in uwht}
            for i, f in enumerate(fmt):
                rate[wht[i]].append(srow[t:t + f].strip())
                t += f

            rate["R"] = [x for x in rate["R"] if x != ""]
            rate["P"] = [x for x in rate["P"] if x != ""]
            for w in "abc":
                rate[w] = float(rate[w][0])
            rate["i"] = int(rate["i"][0])

            if rate["i"] == 1:
                rr = "%e * crate" % rate["a"]
            elif 1 < rate["i"] < 13:
                rr = "%.4e" % rate["a"]
                if rate["b"] != 0e0:
                    rr += " * tgas32**(%.2f)" % rate["b"]
                if rate["c"] != 0e0:
                    rr += " * np.exp(-%.4e*invt)" % rate["c"]
            else:
                continue
            rr = rr.replace("--", "")
            rate["rate"] = rr

            if only is not None:
                has_only = all([x in only for x in rate["R"] + rate["P"]])
                if not has_only:
                    continue

            idx = len(self.reactions)
            rcs = ["n[%s]" % self.sp2idx(x) for x in rate["R"]]
            rate["rhs"] = "k[%d]*" % idx + "*".join(rcs)

            rate["verbose"] = " + ".join(rate["R"]) + " -> " + " + ".join(rate["P"])

            if np.random.random() > fkeep:
                continue

            self.reactions.append(rate)

        if self.verbose > 0:
            print("%d reactions found in %s" % (len(self.reactions), fname))

    # ************************
    @staticmethod
    def sp2idx(arg):
        arg = arg.replace("+", "j")
        arg = arg.replace("-", "k")
        return "idx_" + arg

    # ************************
    def find_species(self):
        self.species = []
        for rate in self.reactions:
            self.species += rate["R"]
            self.species += rate["P"]

        self.species = sorted(list(set(self.species)))
        self.species_idx = [self.sp2idx(x) for x in self.species]

        if self.verbose > 0:
            print("%d species found" % len(self.species))

    # ***********************
    def species_list_to_file(self, fname):
        species_list = ", ".join(["\"%s\"" % x for x in self.species])
        species_list = "species_list = [" + species_list + "]"

        fout = open(fname, "w")
        fout.write(species_list + "\n")
        for i, species in enumerate(self.species):
            fout.write("%s = %d\n" % (self.sp2idx(species), i))

        fout.close()
        if self.verbose > 0:
            print("species list saved to " + fname)

    # ************************
    def preprocess_commons(self):
        body = ""
        body += "crate = 0e0\n"
        body += "tgas = 0e0\n"
        for i, sp in enumerate(self.species_idx):
            body += "%s = %d\n" % (sp, i)

        self.preprocess(body, "commons.py")

    # ************************
    def preprocess_rates(self):
        body = "import numpy as np\n\n"
        body += "def rates(tgas, crate):\n"
        body += "\tk = np.zeros(%d)\n\n" % len(self.reactions)
        body += "\ttgas32 = tgas / 3e2\n"
        body += "\tinvt = 1e0 / tgas\n\n"
        for i, rea in enumerate(self.reactions):
            body += "\t# %s\n" % rea["verbose"]
            body += "\tk[%d] = %s\n\n" % (i, rea["rate"])

        body += "\n\treturn k\n\n"

        self.preprocess(body, "rates.py")

    # ************************
    def preprocess_fex(self):
        body = "import numpy as np\n"
        body += "from commons import *\n\n"
        body += "def fex(t, y, k):\n"
        body += "\tn = 1e1**np.array(y)\n\n"
        body += "\tflux = np.zeros_like(k)\n"
        body += "\tdn = np.zeros_like(n)\n\n"
        for i, react in enumerate(self.reactions):
            body += "\tflux[%d] = %s\n" % (i, react["rhs"])

        body += "\n\n"

        for sp in self.species:
            body += "\tdn[%s] = 0e0" % self.sp2idx(sp)
            for i, rea in enumerate(self.reactions):
                c = rea["R"].count(sp)
                if c > 0:
                    body += "\\\n\t\t-%d*flux[%d]" % (c, i)

                c = rea["P"].count(sp)
                if c > 0:
                    body += "\\\n\t\t+%d*flux[%d]" % (c, i)
            body += "\n"

        body = body.replace("+1*flux[", "+flux[")
        body = body.replace("-1*flux[", "-flux[")

        body += "\n\treturn dn / n * 1e1**t #/ np.log(1e1)\n\n"

        self.preprocess(body, "fex.py")

    # ************************
    @staticmethod
    def preprocess(body, fname):

        fout = open(fname, "w")
        fout.write(body)
        fout.close()
