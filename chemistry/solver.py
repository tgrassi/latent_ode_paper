import matplotlib.pyplot as plt
import numpy as np


class Solver:

    # *******************
    def __init__(self, network):
        self.network = network
        self.neq = len(self.network.species)
        self.spy = 365.*24.*3600.
        self.n = self.krates = None

    # *******************
    def solve(self, n_in, dt, tgas, crate, tsteps=30, tmin=None, atol=1e-8, rtol=1e-4):
        from scipy.integrate import solve_ivp
        from fex import fex
        from rates import rates

        if tmin is None:
            tmin = self.spy * 1e-6

        x0 = np.zeros(self.neq)
        for i, sp in enumerate(self.network.species):
            if sp in n_in:
                x0[i] = n_in[sp]

        k = rates(tgas, crate)
        #tspan = (0e0, dt)
        #teval = np.logspace(np.log10(tmin), np.log10(dt), tsteps)
        tspan = (0, np.log10(dt))
        teval = np.linspace(np.log10(tmin), np.log10(dt), tsteps)

        res = solve_ivp(fex, tspan, x0, method="BDF", args=(k,), t_eval=teval, atol=atol, rtol=rtol)

        # dictionary to store results
        n = dict()

        # do the log of time and avoid error on first time that is zero
        # time_log = np.log10(res.t + res.t[1] / 2e0)
        time_log = res.t
        # normalize time [0, 1] in log space
        n["time"] = (time_log - min(time_log)) / (max(time_log) - min(time_log))
        n["time_org"] = time_log

        # store species (ODE is written to deal already in log space)
        for i, sp in enumerate(self.network.species):
            n[sp] = res.y[i]

        self.n = n
        self.krates = k

        return n

    # *********************
    def results_to_file(self, fname):
        from fex import fex
        nspecs = len(self.network.species)
        nsteps = len(self.n[self.network.species[0]])
        res = np.zeros((nspecs*2, nsteps))

        # store x
        for i, sp in enumerate(self.network.species):
            res[i, :] = self.n[sp][:]
            res[i + nspecs, :] = np.gradient(self.n[sp][:], self.n["time"], edge_order=2)

        # store xdot
        #for j, t in enumerate(self.n["time_org"]):
        #    res[nspecs:2*nspecs, j] = fex(t, res[:nspecs, j], self.krates)

        np.savetxt(fname, res.T)

        print("output saved to " + fname)

    # *********************
    def plot_above(self, nmin=1e-5, fname=None):

        label_size = 16
        tick_label_size = 16
        line_width = 2
        legend_font_size = 6
        colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
        lss = ["-", "--", ":"]

        def chem_latex(arg):
            for j in range(10):
                arg = arg.replace("%d" % j, "$_%d$" % j)
            arg = arg.replace("+", "$^+$").replace("$$", "")
            if arg == "E":
                arg = "e$^-$"
            return arg

        plt.clf()
        nfound = 0
        specs = sorted(self.n.keys())
        vmin = min([min(self.n[sp]) for sp in specs])
        vmax = max([max(self.n[sp]) for sp in specs])
        for sp in specs:
            v = self.n[sp]  # 2e0 * (self.n[sp] - vmin) / (vmax - vmin) - 1e0
            if max(v) < nmin:
                continue
            if "time" in sp:
                continue

            plt.plot(self.n["time"], v, label=chem_latex(sp), lw=line_width, ls=lss[nfound // len(colors)],
                     color=colors[nfound % len(colors)])
            nfound += 1

        ncol = nfound // 6 + 1
        plt.legend(loc="best", fontsize=legend_font_size, ncol=ncol)
        plt.xlabel("$t$", fontsize=label_size)
        plt.ylabel("$x(t)$", fontsize=label_size)
        plt.gca().tick_params(axis='both', which='major', labelsize=tick_label_size)

        if fname is None:
            plt.show()
        else:
            plt.savefig(fname, bbox_inches='tight')
