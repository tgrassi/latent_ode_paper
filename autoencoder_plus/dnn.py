import tensorflow as tf
import numpy as np
from tensorflow.keras import Input, Model, Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.optimizers import Adam
from loss import loss, metric_loss0, metric_loss1, metric_loss2, metric_loss3
from parameters import NUMBER_OF_LATENT_VARIABLES, NUMBER_OF_REACTIONS
from dnn_test import dnn_test


# do training by iterating over epochs
def do_training(x_train, y_train, x_valid, y_valid, x_test, y_test, number_test_epochs=10, batch_size=32,
                learning_rate=1e-5):
    import parameters
    # infer dimensions
    _,  number_physical_variables = x_train.shape

    # create model
    m, encoder, decoder = model(NUMBER_OF_LATENT_VARIABLES, number_physical_variables, learning_rate)

    # nonlocal model_encoder, model_decoder
    parameters.encoder = encoder
    parameters.decoder = decoder

    history = dict()
    for iteration in range(100000):
        # do fit for epochs
        hist = m.fit(x_train, y_train, epochs=number_test_epochs, shuffle=True, batch_size=batch_size,
                     validation_data=(x_valid, y_valid)).history

        # append metrics history
        for k, v in hist.items():
            if k not in history:
                history[k] = []
            history[k] = np.append(history[k], v)

        dnn_test(m, history, iteration, x_test, y_test)


# create dnn model
def model(number_latent_variables, number_physical_variables, learning_rate):
    # define encoder
    input_shape = Input(shape=(number_physical_variables))
    encoded = Dense(32, activation='relu', name="encoder_h0")(input_shape)
    encoded = Dense(16, activation='relu', name="encoder_h1")(encoded)
    encoded = Dense(8, activation='relu', name="encoder_h2")(encoded)
    encoded = Dense(number_latent_variables, activation='tanh', name="encoder_last")(encoded)

    # define decoder
    decoded = Dense(8, activation='relu', name="decoder_first")(encoded)
    decoded = Dense(16, activation='relu', name="decoder_h1")(decoded)
    decoded = Dense(32, activation='relu', name="decoder_h2")(decoded)
    decoded = Dense(number_physical_variables, activation='tanh', name="decoder_last")(decoded)

    # define latent RHS layers
    gex = Gex(number_latent_variables)(encoded)

    # merge the autoencoder and the RHS into single output
    merged = tf.keras.layers.concatenate([decoded, gex], axis=1)

    # prepare model
    m = Model(input_shape, merged)
    m.compile(optimizer=Adam(lr=learning_rate), loss=loss,
              metrics=[metric_loss0, metric_loss1, metric_loss2, metric_loss3])

    # prepare encoder
    encoder = Model(inputs=m.input, outputs=m.get_layer("encoder_last").output)

    # prepare decoder
    decoder = Sequential(name="decoder")
    decoder.add(Input(shape=(number_latent_variables)))

    # add layers to the decoder model
    decoder_first_index = [layer.name for layer in m.layers].index("decoder_first")
    decoder_last_index = [layer.name for layer in m.layers].index("decoder_last")
    for layer in m.layers[decoder_first_index:decoder_last_index+1]:
        print(layer.name)
        decoder.add(layer)
    decoder.build()

    return m, encoder, decoder


# latent ode RHS
class Gex(tf.keras.layers.Layer):
    def __init__(self, units=32):
        super(Gex, self).__init__()
        self.units = units
        self._name = "gex"
        self.batch_size = None
        self.w = None

    def build(self, input_shape):
        self.batch_size = input_shape[0]
        self.w = self.add_weight(
            # shape=(input_shape[-1], self.units),
            shape=(NUMBER_OF_REACTIONS,),
            initializer="random_uniform",
            trainable=True,
        )

    def call(self, inputs):
        # ****************************
        # THIS CODE IS GENERATED WITH network2tensor.py
        # z variables
        z0 = inputs[:, 0:1]  # A
        z1 = inputs[:, 1:2]  # AA
        z2 = inputs[:, 2:3]  # AB
        z3 = inputs[:, 3:4]  # B
        z4 = inputs[:, 4:5]  # BB

        # init species in fluxes (reactants)
        preflux = tf.concat([
         tf.multiply(z0, z0),
         z1,
         tf.multiply(z0, z3),
         z2,
         tf.multiply(z3, z3),
         z4,
         tf.multiply(z1, z4),
         tf.multiply(z2, z2),
         tf.multiply(z1, z3),
         tf.multiply(z2, z0),
         tf.multiply(z4, z0),
         tf.multiply(z2, z3)
        ], axis=1)

        # prepare S matrix
        s = np.array([
         [-2, 2, -1, 1, 0, 0, 0, 0, 1, -1, -1, 1],
         [1, -1, 0, 0, 0, 0, -1, 1, -1, 1, 0, 0],
         [0, 0, 1, -1, 0, 0, 2, -2, 1, -1, 1, -1],
         [0, 0, -1, 1, -2, 2, 0, 0, -1, 1, 1, -1],
         [0, 0, 0, 0, 1, -1, -1, 1, 0, 0, -1, 1]
        ], dtype="float64")

        # END OF CODE GENERATED WITH network2tensor.py
        # ****************************

        p = 1e1**self.w
        flux = tf.multiply(p, preflux)
        xdot = tf.linalg.matvec(s, flux)

        return xdot
