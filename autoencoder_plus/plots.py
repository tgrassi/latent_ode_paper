import numpy as np
import matplotlib.pyplot as plt
import sys
import parameters
from parameters import NUMBER_OF_LATENT_VARIABLES, NUMBER_OF_TIMESTEPS, mass
sys.path.append("../chemistry/")
from species_list import species_list

colors = plt.rcParams['axes.prop_cycle'].by_key()['color']


# ******************************
# save png locally and also in the history folder with iteration number postfix
def save_png_and_copy_last(png_file, iteration):
    plt.savefig(png_file)
    plt.savefig("history/" + png_file.split(".")[0] + "_%06d.png" % iteration)


# ******************************
# plot total and individual losses, for training and validation
def plot_loss(history, iteration):
    loss_names = list(np.sort(np.unique([x.replace("val_", "") for x in history.keys()])))

    # PLOT metrics
    plt.clf()
    for k, v in history.items():
        if k.startswith("val_"):
            ls = "--"
        else:
            ls = "-"
        idx = loss_names.index(k.replace("val_", ""))
        plt.loglog(np.arange(len(v)), v, label=k, color=colors[idx], ls=ls)

    plt.legend(loc="best", ncol=2)
    plt.title("iteration %d" % iteration)
    plt.savefig("loss.png")


# ******************************
# plot autencoder prediction vs input data, i.e. $\bar x$ vs $\bar x'$
# if perfect reconstruction all points should be on the diagonal, i.e. x = x'
def plot_autoencoder_prediction(m, iteration, x_test, y_test, nparts=5):

    y_predict = m.predict(x_test)  # compute x'
    _, number_physical_variables = x_test.shape

    # loop to plot reconstruction plane in the x space
    for j in range(0, number_physical_variables, nparts):
        jmin = j
        jmax = min(j + nparts, number_physical_variables)
        plt.clf()
        # loop to plot autoencoded variables
        for i in range(jmin, jmax):
            plt.scatter(y_predict[:, i], y_test[:, i], s=1, label=species_list[i], color=colors[i % len(colors)])
        plt.plot([-1, 1], [-1, 1], alpha=0.3)

        plt.xlim(-1e0, 1e0)
        plt.ylim(-1e0, 1e0)
        plt.xlabel("$x'$")
        plt.ylabel("$x$")
        plt.legend(loc="best")
        plt.title("iteration %d" % iteration)
        save_png_and_copy_last("autoencoder_%d.png" % j, iteration)


# ******************************
# plot evolution of each latent variable starting from $z(t) = z(t=0)$ and compares with the trajectories computed by
# encoding the original trajectories, i.e. $z(t) = phi(x(t))$.
# if perfect reconstruction the two $z(t)$ should coincide
def plot_evolution(evolution, iteration, x_test):

    z = parameters.encoder(x_test)

    # PLOT z(t) and compare with phi(x(t))
    plt.clf()
    # plot evolution obtained with the solver using gex as RHS
    for i in range(NUMBER_OF_LATENT_VARIABLES):
        label = ""
        if i == 0:
            label = "evolved z(t)"
        plt.plot(evolution.t, evolution.y[i, :], color=colors[i], label=label)
    neval = len(evolution.t)
    tile_mass = np.tile(mass, (neval, 1)).T
    plt.plot(evolution.t, np.sum(evolution.y * tile_mass, axis=0), color="k", label="$\\sum_i z_i$")
    # plot evolution obtained encoding the known evolution in the physical space
    for i in range(NUMBER_OF_LATENT_VARIABLES):
        label = ""
        if i == 0:
            label = "$\\phi(x(t))$"
        plt.plot(evolution.t, z[:neval, i].numpy(), color=colors[i], ls=":", label=label)

    plt.plot(evolution.t, np.sum(z[:neval, :].numpy() * tile_mass.T, axis=1), color="k", ls=":",
             label="$\\sum_i \\phi(x(t))_i$")

    plt.xlabel("time")
    plt.ylabel("$z(t)$")
    plt.legend(loc="best")
    plt.title("iteration %d" % iteration)
    save_png_and_copy_last("zspace_compare.png", iteration)


# ******************************
# plot evolution of $x(t)$ vs $x'(t)$ by using
# 1. decoded evolution $z(t)$ by evolving the system from $z(t=0)$
# 2. autoencoded $x(t)$ (i.e. without evolving the latent ODE system)
# if perfect reconstrution the three curves should match
def plot_xspace(evolution, iteration, x_test, nparts=5):
    _, number_physical_variables = x_test.shape

    neval = NUMBER_OF_TIMESTEPS

    # PLOT psi(z(t)) and compare with x(t), in chunks of nparts variables
    xautoencoded = parameters.decoder(parameters.encoder(x_test))
    xevol = parameters.decoder(evolution.y.T)
    for j in range(0, number_physical_variables, nparts):
        plt.clf()
        jmin = j
        jmax = min(j + nparts, number_physical_variables)
        # plot evolution obtained with the solver using gex as RHS
        for i in range(jmin, jmax):
            label = ""
            if i == jmin:
                label = "evolved $\\psi(z(t))$"
            plt.plot(evolution.t, xevol[:neval, i], color=colors[i % len(colors)], label=label)
        # plot evolution obtained encoding the known evolution in the physical space
        for i in range(jmin, jmax):
            label = "x(t) " + species_list[i]
            plt.plot(evolution.t, x_test[:neval, i], ls=":", color=colors[i % len(colors)], label=label)

        # plot evolution obtained encoding the known evolution in the physical space
        for i in range(jmin, jmax):
            plt.plot(evolution.t, xautoencoded[:neval, i], ls="--", color=colors[i % len(colors)], alpha=0.5)

        plt.xlabel("time")
        plt.ylabel("$x(t)$")

        plt.legend(loc="best", ncol=3)
        plt.title("iteration %d (x: %d  to %d)" % (iteration, jmin, jmax))
        save_png_and_copy_last("xspace_compare_%02d.png" % j, iteration)


# ******************************
def plot_xdot(evolution, iteration, x_test, y_test, nparts=5):

    _, number_physical_variables = x_test.shape
    neval = len(evolution.t)

    # decode evolution of latent ODE, i.e. x(t) = psi(z(t))
    xevol = parameters.decoder(evolution.y.T)

    # PLOT xdot
    for j in range(0, number_physical_variables, nparts):
        plt.clf()
        jmin = j
        jmax = min(j + nparts, number_physical_variables)
        for i in range(jmin, jmax):
            label1 = label2 = ""
            if i == jmin:
                label1 = "$\\dot x$"
                label2 = "grad $\\psi(z(t))$"
            plt.plot(evolution.t, y_test[:neval, number_physical_variables + i], ls="-",
                     color=colors[i % len(colors)], label=label1)
            plt.plot(evolution.t, np.gradient(xevol[:neval, i], evolution.t), ls="--",
                     color=colors[i % len(colors)], label=label2)

        plt.xlabel("time")
        plt.ylabel("$\\dot{\\bar x}$")
        plt.legend(loc="best")
        plt.title("iteration %d (x: %d  to %d)" % (iteration, jmin, jmax))
        save_png_and_copy_last("xgrad_%02d.png" % j, iteration)


# ******************************
def plot_zgrid_evolution(evolution, iteration):
    neval = len(evolution.t)

    plt.clf()
    for i in range(NUMBER_OF_LATENT_VARIABLES):
        for j in range(i+1, NUMBER_OF_LATENT_VARIABLES):
            plt.scatter(evolution.y[i, :neval], evolution.y[j, :neval], label="%d, %d" % (i, j), s=1)
    plt.xlabel("$z_i$")
    plt.ylabel("$z_j$")
    plt.legend(loc="best", ncol=2)
    plt.title("iteration %d" % iteration)
    save_png_and_copy_last("zgrid_evolution.png", iteration)
