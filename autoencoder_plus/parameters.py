import numpy as np
NUMBER_OF_REACTIONS = 12
NUMBER_OF_LATENT_VARIABLES = 5
NUMBER_OF_TIMESTEPS = 100
mass = np.array([1e0, 2e0, 2e0, 1e0, 2e0])

lambda0 = 1e0  # loss0 weight, autoencoder reconstruction
lambda1 = 1e-3  # loss1 weight, xdot vs dpsi_dz
lambda2 = 1e-2  # loss2 weight, zdot vs dphi_dx
lambda3 = 1e-3  # loss3 weight, mass conservation latent space

encoder = None
decoder = None
