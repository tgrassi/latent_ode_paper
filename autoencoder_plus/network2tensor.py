import numpy as np

fname = "network.dat"
tabs = 2
tab = " "*4

# read reactions from latent network file
specs = []
reacts = []
do_reverse = False
for row in open(fname):
    srow = row.strip()
    if srow == "" or srow.startswith("#"):
        continue

    if srow == "@reverse":
        do_reverse = True
        continue
        
    print(srow)

    rr, pp = [x.strip() for x in srow.split("->")]
    rr = [x.strip() for x in rr.split(" + ")]
    pp = [x.strip() for x in pp.split(" + ")]

    reacts.append({"R": rr, "P": pp})
    if do_reverse:
        reacts.append({"R": pp, "P": rr})

    specs += rr
    specs += pp


# sort and unique species
specs = sorted(list(set(specs)))

nspecs = len(specs)  # number of species
nrea = len(reacts)  # number of reactions

# loop to write the variables as z0, z1, ...
dspecs = dict()
print(tab*tabs + "# ****************************")
print(tab*tabs + "# THIS CODE IS GENERATED WITH network2tensor.py")
print(tab*tabs + "# z variables")
for i, sp in enumerate(specs):
    xx = "z%d = inputs[:, %d:%d]  # %s" % (i, i, i+1, sp)
    dspecs[sp] = "z%d" % i
    print(tab*tabs + xx)
print("")

# loop to write the variables as x0, x1, ...
print(tab*tabs + "# init species in fluxes (reactants)")
print(tab*tabs + "preflux = tf.concat([")
prefluxes = []
for i, rea in enumerate(reacts):
    if len(rea["R"]) == 2:
        r0, r1 = rea["R"]
        preflux = "tf.multiply(%s, %s)" % (dspecs[r0], dspecs[r1])
    else:
        preflux = "%s" % dspecs[rea["R"][0]]

    prefluxes.append(" " + preflux)

print(tab*tabs + (",\n" + tab*tabs).join(prefluxes))
print(tab*tabs + "], axis=1)\n")

print(tab*tabs + "# prepare S matrix")
s = np.zeros((nspecs, nrea))
for i, rea in enumerate(reacts):
    for r in rea["R"]:
        s[specs.index(r), i] -= 1
    for p in rea["P"]:
        s[specs.index(p), i] += 1

rows = []
for i in range(nspecs):
    rows.append(tab*tabs + " [%s]" % ", ".join(["%d" % x for x in s[i, :]]))

print(tab*tabs + "s = np.array([")
print(",\n".join(rows))
print(tab*tabs + "], dtype=\"float64\")\n")

print(tab*tabs + "# END OF CODE GENERATED WITH network2tensor.py")
print(tab*tabs + "# ****************************")

print("number of species:", nspecs)
print("number of reactions:", nrea)

