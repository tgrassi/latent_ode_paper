from glob import glob
import numpy as np
import sys


# *************************+
# load time evolution data from files
# ARGUMENTS:
# number_train_fraction: fraction of model assigned to training
# number_valid_fraction: fraction of model assigned to valid
# number_models_to_load: number of models to load, if None loads all
# folder: where to look for models
#
# RETURNS: x_train, y_train, x_valid, y_valid, x_test, y_test
# x_*: contains $\bar x$, the physical variables at each time evaluation for each model
# y_*: contains $\bar x$ and \dot{\bar x}, the physical variables (and its derivatives) as above
# x_*.shape: [number_of_models*NUMBER_OF_TIMESTEPS, number_of_physical_variables]
# y_*.shape: [number_of_models*NUMBER_OF_TIMESTEPS, 2*number_of_physical_variables]
def load_data(number_train_fraction=0.9, number_valid_fraction=0.09, number_models_to_load=None,
              folder="../chemistry/outputs"):
    # load data (x, xdot) from chemistry for each model (different initial conditions)
    gmodels = sorted(glob(folder + "/chemistry_*.dat"))
    if number_models_to_load is not None:
        gmodels = gmodels[:number_models_to_load]

    # find variables from data
    number_models = len(gmodels)
    number_timesteps, number_physical_variables = np.loadtxt(gmodels[0]).shape
    number_physical_variables //= 2

    # print variables
    print("Number of models:", number_models)
    print("Number of time-steps", number_timesteps)
    print("Number of physical variables", number_physical_variables)

    # prepare empty dataset
    data = np.zeros((number_timesteps, number_physical_variables * 2, number_models))

    # load each model individually and add to the dataset
    for imod, gfile in enumerate(gmodels):
        data[:, :, imod] = np.loadtxt(gfile)

    # normalize data set in range [-1, 1]
    dmin = np.amin(data[:, :number_physical_variables, :])
    dmax = np.amax(data[:, :number_physical_variables, :])
    data[:, :number_physical_variables, :] = 2e0 * (data[:, :number_physical_variables, :] - dmin) \
                                             / (dmax - dmin) - 1e0
    data[:, number_physical_variables:, :] *= 2e0 / (dmax - dmin)  # normalization of derivative (xdot)

    # calculate the number of training, validation, and test sets
    number_train = int(number_train_fraction * number_models)
    number_valid = int(number_valid_fraction * number_models)
    number_test = number_models - number_valid - number_train

    # print size of sets
    print("Training data:", number_train)
    print("Valid data", number_valid)
    print("Test data", number_test)

    # avoid negative size
    if number_test <= 0:
        sys.exit("ERROR: negative number tests!")

    return split_models(data, number_train, number_valid, number_test)


# **************************++
# divide models into sets
def split_models(data, number_train, number_valid, number_test):
    # infer size from data structure
    number_timesteps, number_physical_variables, _ = data[:, :, :].shape

    # actual number of physical variables is half, since x and xdot are stored
    number_physical_variables //= 2

    # define training set, [nsamples, x] (note chemical models are stacked)
    x_train = np.zeros((number_timesteps * number_train, number_physical_variables))
    y_train = np.zeros((number_timesteps * number_train, 2 * number_physical_variables))

    # define validation set, [nsamples, x] (note chemical models are stacked)
    x_valid = np.zeros((number_timesteps * number_valid, number_physical_variables))
    y_valid = np.zeros((number_timesteps * number_valid, 2 * number_physical_variables))

    # define test set, [nsamples, x] (note chemical models are stacked)
    x_test = np.zeros((number_timesteps * number_test, number_physical_variables))
    y_test = np.zeros((number_timesteps * number_test, 2 * number_physical_variables))

    # loop on models to stack data into training set.
    # note that x_train is [x], while y_train is [x, xdot].
    # since zdot is not available, but the loss uses xdot, the latter is stored in y_*
    for imod in range(number_train):
        x_train[imod * number_timesteps:(imod + 1) * number_timesteps, :] = \
            data[:, :number_physical_variables, imod]  # x
        y_train[imod * number_timesteps:(imod + 1) * number_timesteps, :number_physical_variables] = \
            data[:, :number_physical_variables, imod]  # x, xdot
        y_train[imod * number_timesteps:(imod + 1) * number_timesteps, number_physical_variables:] = \
            data[:, number_physical_variables:, imod]  # x, xdot

    for imod in range(number_valid):
        x_valid[imod * number_timesteps:(imod + 1) * number_timesteps, :] = \
            data[:, :number_physical_variables, number_train + imod]  # x
        y_valid[imod * number_timesteps:(imod + 1) * number_timesteps, :number_physical_variables] = \
            data[:, :number_physical_variables, number_train + imod]  # x, xdot
        y_valid[imod * number_timesteps:(imod + 1) * number_timesteps, number_physical_variables:] = \
            data[:, number_physical_variables:, number_train + imod]  # x, xdot

    for imod in range(number_test):
        x_test[imod * number_timesteps:(imod + 1) * number_timesteps, :] = \
            data[:, :number_physical_variables, number_train + number_valid + imod]  # x
        y_test[imod * number_timesteps:(imod + 1) * number_timesteps, :number_physical_variables] = \
            data[:, :number_physical_variables, number_train + number_valid + imod]  # x, xdot
        y_test[imod * number_timesteps:(imod + 1) * number_timesteps, number_physical_variables:] = \
            data[:, number_physical_variables:, number_train + number_valid + imod]  # x, xdot

    return x_train, y_train, x_valid, y_valid, x_test, y_test
