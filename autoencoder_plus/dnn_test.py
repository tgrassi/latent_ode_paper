import tensorflow as tf
import numpy as np
from plots import plot_loss, plot_autoencoder_prediction, plot_evolution, plot_xspace, plot_xdot, plot_zgrid_evolution
from parameters import NUMBER_OF_LATENT_VARIABLES, NUMBER_OF_TIMESTEPS


# *******************
# test the dnn and do all the plots
def dnn_test(m, history, iteration, x_test, y_test):
    plot_loss(history, iteration)
    plot_autoencoder_prediction(m, iteration, x_test, y_test)
    evolution = evolve_z(m, x_test)
    if evolution is not None:
        plot_evolution(evolution, iteration, x_test)
        plot_xspace(evolution, iteration, x_test)
        plot_xdot(evolution, iteration, x_test, y_test)
        plot_zgrid_evolution(evolution, iteration)


# *******************
# RHS in latent space computed using gex layer
def fex(t, y, m):

    yb = np.zeros((1, NUMBER_OF_LATENT_VARIABLES))
    yb[0, :] = y
    yt = tf.convert_to_tensor(yb)
    dy = m.get_layer("gex")(yt)

    return dy[0, :].numpy()


# *******************
# define some time grid to integrate equations in the latent space
def get_time_grid(tmin=0e0, tmax=1e0):
    return np.linspace(tmin, tmax, NUMBER_OF_TIMESTEPS)


# *******************
# start from z(t=0) = encoded(x(t=0)) and evolve z(t) using the current ODE system g
def evolve_z(m, x_test):
    from scipy.integrate import solve_ivp
    import parameters
    # SOLVE ODE IN THE LATENT SPACE
    # compute encoded data
    z = parameters.encoder(x_test)

    # initial values at t=0 in the latent space
    z0 = z[0, :].numpy()

    # get a time grid for integration
    time_grid = get_time_grid()

    try:
        print("solving ODE in the latent space...")
        print("z0", z0)
        print("gex(z0)", fex(0e0, z0, m))
        # evolve the system using the RHS from gex
        solution = solve_ivp(fex, [0e0, time_grid[-1]], z0,
                             t_eval=time_grid, method="BDF", atol=1e-10, rtol=1e-6, args=[m])
    except Exception as ex:
        print("WARNING: solver failed!", ex)
        return None

    # check if solver worked
    if not solution.success:
        print("ERROR: BDF!")
        return None

    # return true if not fails
    return solution
