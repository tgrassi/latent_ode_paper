import tensorflow as tf
from load_data import load_data
from dnn import do_training

# use double-precision (ivp solver uses double precision)
tf.keras.backend.set_floatx('float64')

# load models from the ../chemistry/outputs folder
x_train, y_train, x_valid, y_valid, x_test, y_test = load_data()

# do training (and test+plots)
do_training(x_train, y_train, x_valid, y_valid, x_test, y_test, number_test_epochs=30, learning_rate=1e-4)

