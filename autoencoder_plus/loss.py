import tensorflow as tf
import numpy as np
import sys
import parameters
from parameters import mass, lambda0, lambda1, lambda2, lambda3


# loss function
# y_actual = [x, xdot] <- known
# y_predict = [x, zdot] <- predicted by m(x)
def loss(y_actual, y_predict, what=None):
    _, number_physical_variables = y_actual.shape
    number_physical_variables //= 2

    x = y_actual[:, :number_physical_variables]  # input and actual x are the same (autoencoder definition)
    xpredict = y_predict[:, :number_physical_variables]  # x predicted by autoencoder, e.g. psi(phi(x))
    zdot = y_predict[:, number_physical_variables:]  # zdot predicted by gex(z)
    xdot = y_actual[:, number_physical_variables:]  # known xdot (from chemical model)

    # compute latent space
    z = parameters.encoder(x)

    # find jacobian of encoder in dx
    with tf.GradientTape() as tape:
        tape.watch(x)
        zenc = parameters.encoder(x)
    dphi_dx = tape.batch_jacobian(zenc, x)

    # find jacobian of decoder in dz
    with tf.GradientTape() as tape:
        tape.watch(z)
        xauto = parameters.decoder(z)
    dpsi_dz = tape.batch_jacobian(xauto, z)

    # weight autoencoder reconstruction for some selected species
    chem_weights = np.ones(number_physical_variables) * 1e-1
    # chem_weights[idx_CO] = 1e0
    chem_weights[:] = 1e0  # FIXME
    chem_weights = tf.constant(chem_weights)

    # compute autoencoder reconstruction L2 loss
    squared_diff = tf.math.squared_difference(x, xpredict)
    weighted_squared_diff = tf.multiply(squared_diff, chem_weights)
    loss0 = lambda0 * tf.reduce_mean(weighted_squared_diff)

    # compute autoencoder reconstruction L2 loss decoder gradient reconstruction
    loss1 = lambda1 * tf.reduce_mean(tf.math.squared_difference(xdot, tf.linalg.matvec(dpsi_dz, zdot)))
    # compute autoencoder reconstruction L2 loss encoder gradient reconstruction
    loss2 = lambda2 * tf.reduce_mean(tf.math.squared_difference(zdot, tf.linalg.matvec(dphi_dx, xdot)))

    # latent variables conservation
    loss3 = lambda3 * (tf.abs(tf.reduce_sum(mass*tf.linalg.matvec(dphi_dx, xdot))) + tf.abs(tf.reduce_sum(mass*zdot)))

    # total loss
    if what is None:
        return loss0 + loss1 + loss2 + loss3
    elif what == 0:
        return loss0
    elif what == 1:
        return loss1
    elif what == 2:
        return loss2
    elif what == 3:
        return loss3
    else:
        sys.exit("ERROR: unknown loss number %d" % what)


# define specific loss metrics
def metric_loss0(y_a, y_p):
    return loss(y_a, y_p, what=0)


def metric_loss1(y_a, y_p):
    return loss(y_a, y_p, what=1)


def metric_loss2(y_a, y_p):
    return loss(y_a, y_p, what=2)


def metric_loss3(y_a, y_p):
    return loss(y_a, y_p, what=3)
